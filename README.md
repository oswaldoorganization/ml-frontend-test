# ml-frontend-test

Este repositorio sirve como cliente web a la solucion de items al estilo de tienda on-line. Es creada solo con fines de prueba tecnica.
Es un cliente web que se adapta a pantallas pequeñas como los celulares o tabletas.

ver tambien el [componente backend Api Rest](https://gitlab.com/oswaldoorganization/ml-rest-service-test/-/blob/master/README.md "Repo backend")

El app tiene como conexión predeterminada el backend de pruebas:

```
https://ml-rest-app.herokuapp.com/
```

### App version

Versión 1.0


## Lenguajes & frameworks

- javascript (ES6)
- Vue 2
- [Vue Cli](https://cli.vuejs.org/ "vue cli") 
- sass, scss
- html


## Configuración prueba local
Una vez tenga instalado Vue Cli y clonado el proyectoc installar las dependencias con: 
```
npm install
```

### Compilar y "hot-reloads" desarrollo
El siguiente comando despliega la palicación en su browser en modo de desarrollo, esto incluye "hot-reloads" que permite ver los cambios realizados sin recargar la página
```
npm run serve
```

### Compilar para producción
La compilación para ambiente de producción "minifica" el código y empaqueta el app solo con las dependencias necesarias:
```
npm run build
```

### Personalizar configuration
Ver [referencia de configuración del CLI](https://cli.vuejs.org/config/).

## Docker

Si prefiere construir una imagen docker y correr la aplicacion: 

`docker build -t [image-name]:v0.1 .`

confirmar la imagen con:

`docker images`

correr un contenedor basado en la imagen:

`docker run -it -p 8080:80 --rm --name <name_instance> [image-name]:v0.1`


