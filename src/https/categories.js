import service from './../https/service'

const getCategories = (token) => {
  const options = {
    method: 'get',
    url: `categories`,
    headers: {
      'Authorization': `Bearer ${token}`
    }
  }
  return service(options)
}

export default getCategories