import service from './../https/service'

const getItem = (id, token) => {
  const options = {
    method: 'get',
    url: `items/${id}`,
    headers: {
      'Authorization': `Bearer ${token}`
    }
  }
  return service(options)
}

const getItemDescription = (id, token) => {
  const options = {
    method: 'get',
    url: `items/${id}/description`,
    headers: {
      'Authorization': `Bearer ${token}`
    }
  }
  return service(options)
}

const getItems = (text, token) => {
  const options = {
    method: 'get',
    url: `items/?search=${text}`,
    headers: {
      'Authorization': `Bearer ${token}`
    }
  }
  return service(options)
}

export default {getItems, getItem, getItemDescription}