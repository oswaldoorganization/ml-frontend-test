const axios = require('axios')
const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_URL_SERVICE
})

export default service