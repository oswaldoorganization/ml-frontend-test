import service from './../https/service'

const login = (email, password) => {
  const options = {
    method: 'post',
    url: 'users/login',
    headers: {
      email,
      password
    }
  }
  return service(options)
}

export default login