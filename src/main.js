import '@babel/polyfill'
import 'mutationobserver-shim'
import Vue from 'vue'
import store from './store/index'
import './plugins/bootstrap-vue'
import App from './App.vue'
import router from './router'
import './filters/index'

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App),
  store
}).$mount('#app')
