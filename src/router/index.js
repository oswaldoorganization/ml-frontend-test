import Vue from 'vue'
import VueRouter from 'vue-router'
import NotFound from '@/views/NotFound'
import Items from '@/views/items'
import Details from '@/views/Details'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home'
  },
  {
    path: '/items',
    component: Items,
  },
  {
    path: '/items/:id',
    name: 'items',
    component: Details
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },

  {
    path: '/404',
    component: NotFound
  },
  {
    path: '/*',
    redirect: '/404'
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
