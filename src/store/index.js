import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    userPayload: '',
    token: '',
    items: [],
    textSearch: ''
  },
  mutations: {
    setTextSearch (state, text) {
      state.textSearch = text
    },
    setToken (state, token) {
      state.token = token
    },
    setUser (state, payload) {
      state.userPayload = payload
    },
    setItems (state, collectionItems) {
      state.items = collectionItems
    }
  }
})

export default store